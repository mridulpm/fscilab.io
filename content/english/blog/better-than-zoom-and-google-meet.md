---
title: "Invitation for Webinar on June 13: Better Than Zoom and Google Meet"
date: 2021-06-02T23:58:52+05:30
author: "Arun Mathai S.K."
author_about: "https://arunmathaisk.in/"
draft: false
tags: ["privacy", "jitsi", "video-conferencing", "webinar"]
---
Hi all,

We are going to conduct a webinar to discuss video conferencing solutions (specifically Jitsi Meet).

Now, you may ask isn’t a webinar an overkill??

Like aren’t there tens of videos explaining what Jitsi Meet is and why it is so much better.

Well, you are right, But we mean to accomplish something different.

We mean to include Jitsi Meet not only as a cool secure alternative to other services but one which an individual/community can host and one which truly respects the term “privacy”.

We intend to have a personal connection with the attendees, have a live QnA, share our own personal stories as to why we use Jitsi Meet and lastly but mainly tell them about our own fund-raising campaign (https://fund.fsci.in) for our own administered Jitsi Meet instance (https://meet.fsci.in).

We simply don't want people to contribute(well that is also cool )but we want people to know how the money they contribute helps us volunteers in running secure freedom respecting Jitsi instance.

And I would also like to use the cliché line….

“Thus helping make this world a better place”.

Read more - https://fsf.org.in/article/better-than-zoom/

The invitation is open to anyone interested in learning about privacy respecting video conferencing services. The webinar will be non-technical in nature. Technical details can be provided if the participants are interested.
